import pygame
from pygame import draw, display, Vector2
from pygame import event, key
from pygame.constants import K_SPACE, K_LEFT, K_RIGHT, K_UP, K_DOWN, K_q, K_w, K_e
import requests
from json import dumps
import random
import math

pygame.init()
URL = "http://127.0.0.1:5000"
nb_frame = 1000


class Place:
    def __init__(self):
        self.screen = display.set_mode(((30*30), (30*30)+51))
        self.tableau = [[0 for i in range(30)] for i in range(30)]
        self.palette = Palette(50, 30*30, self.screen)
        icon = pygame.image.load("./icon.png")
        display.set_icon(icon)

    # POST la couleur (en indice) au serveur
    def envoi_couleur(self, couleur, position):
        print("appelé", couleur, position)
        self.tableau[position[0]][position[1]] = couleur
        coulrgb = self.palette.couleurs_rgb[couleur]
        case = pygame.Rect(position[0]*(self.screen.get_width()/30),
                           (position[1] * (self.screen.get_height()-51)/30)+51, 30, 30)
        draw.rect(
            self.screen,
            coulrgb,
            case,  # La case à afficher
        )
        display.update(case)
        # Le corps d'une requête au format dict aussi
        body = {'x': position[0], 'y': position[1], 'color': couleur}
        # Headers HTTP pour indiquer le format au serveur
        headers = {'content-type': 'application/json'}
        requests.post(
            URL+'/place',
            dumps(body),
            headers=headers
        )

    # Converti une position de surface en une position dans la grille
    def surface_a_grille(self, position):
        x = math.floor((position[0]/self.screen.get_width())*30)
        y = math.floor(((position[1]-51)/(self.screen.get_height()-51))*30)
        return (x, y)

    def raffraichi_grille(self):
        print("...rafraichissement de la grille")
        for i in range(30):
            for j in range(30):
                case = pygame.Rect(i*(self.screen.get_width()/30),
                                   (j * (self.screen.get_height()-51)/30)+51, 30, 30)
                couleur = self.palette.couleurs_rgb[self.tableau[i][j]]
                draw.rect(
                    self.screen,
                    couleur,
                    case,  # La case à afficher
                )
        display.update()

    def raffraichi_tableau(self):
        print("...rafraichissement du tableau")
        res = requests.get(URL+'/full').json()
        self.tableau = res
        print(res)

    def run(self):
        print("running...")
        running = True
        self.screen.fill((0, 0, 0))
        self.palette.dessine()
        compteur_frame = 0
        while running:
            if compteur_frame % nb_frame == 0:
                self.raffraichi_tableau()
                self.raffraichi_grille()
                compteur_frame = 0
            for e in event.get():
                if e.type == pygame.MOUSEBUTTONDOWN:
                    couleur_i = self.palette.couleur_position(e.dict["pos"])
                    print(couleur_i)
                    if not self.palette.position_dans_palette(e.dict["pos"]):
                        coord_grille = self.surface_a_grille(e.dict["pos"])
                        self.envoi_couleur(couleur_i, coord_grille)
                if e.type == pygame.QUIT:
                    running = False
                    # jeu_fini = False
            display.update()
            compteur_frame += 1


class Palette:
    def __init__(self, hauteur, largeur, screen):
        self.hauteur = hauteur
        self.largeur = largeur
        self.screen = screen
        self.couleurs = [i for i in range(10)]
        self.couleurs_rgb = [(0, 0, 0)for i in range(10)]
        self.couleur_courante = 0
        for i in range(10):
            self.couleurs_rgb[i] = (random.randint(
                0, 255), random.randint(0, 255), random.randint(0, 255))

    # dessine la palette à l'écran
    def dessine(self):
        for i in range(10):
            case_couleur = pygame.Rect(
                i*(self.largeur/10), 0, self.largeur/10, self.hauteur)
            draw.rect(
                self.screen,
                self.couleurs_rgb[i],
                case_couleur,  # La raquette
            )

    # renvoi l'indice de couleur correspond à une position à l'écran
    def couleur_position(self, mouse_pos):
        if mouse_pos[1] > self.hauteur:
            return self.couleur_courante
        self.couleur_courante = math.floor((mouse_pos[0]/self.largeur)*10)
        return self.couleur_courante

    # True si la position de souris se trouve dans la palette, False sinon
    def position_dans_palette(self, mouse_pos):
        return mouse_pos[1] < self.hauteur


prog = Place()
prog.run()


# # Ici on récupère un dictionnaire de valeurs
# res = requests.get(URL+'/hello').json()
# # Le corps d'une requête au format dict aussi
# body = {'value': res['name']}
# # Headers HTTP pour indiquer le format au serveur
# headers = {'content-type': 'application/json'}
# # Ici on effectue un POST avec notre body en JSON
