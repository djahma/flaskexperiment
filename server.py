from flask import Flask, request, jsonify, make_response
from flask import render_template
app = Flask(__name__)

# Definition: [x][y]=color
tableau = [[0 for i in range(30)] for i in range(30)]


# @app.route('/hello/')
# @app.route('/hello/<name>')
# def hello(name=None):
#     return render_template('hello.html', name=name)

@app.route('/hello/', methods=['POST'])
def hello():
    body = request.json
    return body['name']


@app.route('/place', methods=['POST'])
def place():
    body = request.json
    print(body)
    x = int(body['x'])
    y = int(body['y'])
    color = int(body['color'])
    tableau[x][y] = color
    return jsonify(tableau)


@app.route('/full', methods=['GET'])
def full():
    return jsonify(tableau)


app.run(host='0.0.0.0')
